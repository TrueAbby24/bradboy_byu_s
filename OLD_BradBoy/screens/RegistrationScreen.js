import React from 'react';
import { 
    View, 
    Text, 
    Button, 
    TouchableOpacity, 
    TextInput, 
    StyleSheet, 
    Alert, 
    ScrollView,
    Image,
    Title
 } from 'react-native';
import { Constants, } from 'expo'
import { 
    createAppContainer, 
    createStackNavigator, 
    StackActions, 
    NavigationActions 
} from 'react-navigation';
import * as Font from 'expo-font';


export default class RegistrationScreen extends React.Component {
    static navigationOptions = {
      title: 'Registration Screen',
      headerMode: 'screen',
      cardStyle: { backgroundColor: '#6ab04c' },
    };

    state = {email: '', password: '',};

    onLogin() {
      const { email, password } = this.state;
      Alert.alert('Your credentials were:', `e-mail: ${email} \n Pass: ${password}`);
    }

    render() {
        return (
        <ScrollView contentContainerStyle={styles.center}>
          <Image
          style={{marginTop: 40, height: '70%', width: '70%'}}
          source={require('../assets/images/bradboy.png')}
          />
          <Text style={styles.space}></Text>
          <Text style={styles.space}></Text>
          <Text style={styles.space}></Text>
            <TextInput
              value={this.state.email}
              keyboardType = 'email-address'
              onChangeText={(email) => this.setState({ email })}
              placeholder='e-Mail'
              placeholderTextColor = 'white'
              style={styles.input}
            />
            <TextInput
              value={this.state.password}
              onChangeText={(password) => this.setState({ password })}
              placeholder={'Password'}
              secureTextEntry={true}
              placeholderTextColor = 'white'
              style={styles.input}
            />
            <Text style={styles.space}></Text>
            <TouchableOpacity
              style={styles.button}
              onPress={this.onLogin.bind(this)}
           >
             <Text style={styles.buttonText}>Go!</Text>
           </TouchableOpacity>
        </ScrollView>
        );
      }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#6ab04c',
    },
    titleText:{
        //fontFamily: 'Baskerville',
        marginTop: 20,
        fontSize: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'powderblue',
        width: '70%',
        height: 50,
        padding: 10,
        marginTop: 10,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 25,
        marginBottom: 10,
    },
    buttonText:{
        //fontFamily: 'Baskerville',
        fontSize: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        width: '70%',
        //fontFamily: 'Baskerville',
        fontSize: 20,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 25,
        marginVertical: 10,
    },
    space: {
        width: 200,
        height: 25,
        padding: 0,
    },
    bigSpace: {
      width: 200,
      height: 100,
      padding: 0,
  },
    center: {
        flex: 0,
        alignItems: 'center',
        backgroundColor: '#6ab04c',
        width: '100%',
    },

});

RegistrationScreen.navigationOptions = {
  title: 'Login or Join Now!',
};
