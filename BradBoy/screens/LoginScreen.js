import React from 'react';
import { Header, Left, Icon, Container, Content, Right, Title } from "native-base"
import { DrawerActions } from 'react-navigation';
import { 
    View, 
    Text, 
    Button, 
    TouchableOpacity, 
    TextInput, 
    StyleSheet, 
    Alert, 
    ScrollView,
    Image,
 } from 'react-native';


export default class LoginScreen extends React.Component {
    constructor() {
        super()
        this.state = {
          email: '',
          password: ''
        }
    }
    static navigationOptions = {
      title: 'My Account',
      headerMode: 'screen',
      cardStyle: { backgroundColor: '#6ab04c' },
    };

    onLogin() {
      const { email, password } = this.state;
      if (email == "" && password == "") {
        Alert.alert('Please enter your email and password.');
      } else if (email == "") {
        Alert.alert('Please enter your email address.');
      } else if (password == "") {
        Alert.alert('Please enter your password.');
      } else {
          fetch('http://byusoftware.uqcloud.net/registration.php', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              email: this.state.email,
              password: this.state.password
            })
          }).then((response) => response.json()).then((responseJson) => {
          
              //Showing response message coming from server after inserting records.
              Alert.alert(responseJson);
            }).catch((error) => {
                console.error(error);
            });
        }
      }

    render () {
      return (
          <Container>
              <Header>
                  <Left>
                      <Icon name="ios-menu" onPress={() => 
                          Alert.alert('You must login first prior to navigating the app.')} />
                  </Left>
                  <Title style={styles.title}>Login</Title>
                  <Right />
              </Header>
              <Content contentContainerStyle={styles.container}>
                <Image
                  style={{height: '38%', width: '75%', 
                  alignSelf: "center", marginBottom: "20%"}}
                  source={require('../assets/images/bradboy.png')}
                />
                <TextInput
                  value={this.state.email}
                  keyboardType = 'email-address'
                  onChangeText={(email) => this.setState({ email })}
                  placeholder='e-Mail'
                  placeholderTextColor = 'white'
                  style={styles.input}
                  onSubmitEditing={() => { this.secondTextInput.focus(); }}
                  autoCapitalize={'none'}
                />
                <TextInput
                  value={this.state.password}
                  onChangeText={(password) => this.setState({ password })}
                  placeholder={'Password'}
                  secureTextEntry={true}
                  placeholderTextColor = 'white'
                  style={styles.input}
                  ref={(input) => { this.secondTextInput = input; }}
                  autoCapitalize={'none'}
                />
                <TouchableOpacity
                  style={styles.button}
                  onPress={this.onLogin.bind(this)}>
                <Text style={styles.buttonText}>Go!</Text>
                </TouchableOpacity>
                <Text style={styles.linkText} onPress={ 
                  ()=> this.props.navigation.navigate('Settings')}> Don't have an account?</Text>
              </Content>
          </Container>
      );
  }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ffbe76',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
        alignContent: "center",
        alignItems: "center", 
        textAlign: "center"
    },
    linkText: {
      marginTop: '2%',
      alignContent: "center",
      alignItems: "center", 
      textAlign: "center",
      color: 'blue',
      textDecorationLine: 'underline'
  },
    titleText:{
        //fontFamily: 'Baskerville',
        marginTop: 20,
        fontSize: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'powderblue',
        width: '70%',
        height: 50,
        padding: 10,
        marginTop: 10,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 5,
        marginBottom: 10,
  },
    buttonText:{
        //fontFamily: 'Baskerville',
        fontSize: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        width: '70%',
        //fontFamily: 'Baskerville',
        fontSize: 20,
        color: 'white',
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 5,
        marginVertical: 10,
    },
    space: {
        width: 200,
        height: 25,
        padding: 0,
    },
    bigSpace: {
      width: 200,
      height: 100,
      padding: 0,
  },
    center: {
        flex: 0,
        alignItems: 'center',
        backgroundColor: '#6ab04c',
        width: '100%',
    },
    title: {
        alignSelf: "center",
    }

});

