import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native"
import { Icon, Button, Container, Header, Content, Left, Title , Right } from "native-base"
import { DrawerActions } from 'react-navigation';

export default class Homescreen extends React.Component {

    /*static navigationOptions = {
        drawerIcon:(
            <Image source={require('ios-home')}
            style={{ height: 24, width: 24}} />
        )
    }*/
    render () {
        return (
            <Container>
                <Header>
                    <Left>
                        <Icon name="ios-menu" onPress={() => 
                            this.props.navigation.dispatch(DrawerActions.openDrawer())} />
                    </Left>
                    <Title style={styles.title}>Home</Title>
                    <Right />
                </Header>
                <Content contentContainerStyle={styles.container}>
                    <Text style={styles.text}>This is the home screen :)</Text>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
        alignContent: "center",
        alignItems: "center", 
        textAlign: "center"
    },
    title: {
        alignSelf: "center",
    }
  });