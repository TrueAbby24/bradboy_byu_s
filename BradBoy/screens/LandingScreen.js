import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native"
import { Icon, Button, Container, Header, Content, Left, Title , Right } from "native-base"
import { DrawerActions } from 'react-navigation';

export default class LandingScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        let drawerLockMode = 'locked-closed';
         return {
            drawerLockMode,
         };
       }

    render () {
        return (
            <Container>
                <Content contentContainerStyle={styles.container}>
                    <Image
                        style={{height: '38%', width: '75%', 
                        alignSelf: "center", marginBottom: "20%"}}
                        source={require('../assets/images/bradboy.png')}
                    />
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('Login')}>
                        <Text style={styles.buttonText}>Login</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('Login')}>
                        <Text style={styles.buttonText}>Register</Text>
                    </TouchableOpacity>
                </Content>
            </Container>
        );
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ffbe76',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
        alignContent: "center",
        alignItems: "center", 
        textAlign: "center"
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'powderblue',
        width: '70%',
        height: 50,
        padding: 10,
        marginTop: 10,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 5,
        marginBottom: 10,

    },
    buttonText:{
        //fontFamily: 'Baskerville',
        fontSize: 20,
        alignItems: 'center',
        justifyContent: 'center',
    }
});