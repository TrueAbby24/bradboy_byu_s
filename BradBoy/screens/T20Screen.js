import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native"
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { Icon, Button, Container, Header, Content, Left, Title , Right } from "native-base"
import { DrawerActions } from 'react-navigation';

export default class T20Screen extends React.Component {
    state = {
        timerOn: true,
        timerStart: 0,
        timerTime: 0,
        tableHead: ['No', 'Score','FOW'],
        tableData: [
          ['1'],
          ['2'],
          ['3'],
          ['4'],
          ['5'],
          ['6'],
          ['7'],
          ['8'],
          ['9'],
          ['10'],
          ['11']
        ]
      };

      startTimer = () => {
        this.setState({
          timerOn: true,
          timerTime: this.state.timerTime,
          timerStart: this.state.timerTime
        });
        this.timer = setInterval(() => {
          const newTime = this.state.timerTime - 10;
          if (newTime >= 0) {
            this.setState({
              timerTime: newTime
            });
          } else {
            clearInterval(this.timer);
            this.setState({ timerOn: false });
            alert("The session is over!");
          }
        }, 10);
      };

      stopTimer = () => {
        clearInterval(this.timer);
        this.setState({ timerOn: false });
      };

    render(){
        const state = this.state;
        return(
            <Container>
                <Button onPress = {this.timer} Title = "Play"
                />
                <Button onPress = {this.stopTimer} Title = "Stop"/>
                <Table>
                <Row data={state.tableHead}/>
                <Rows data={state.tableData}/>
                </Table>
            </Container>
        )
    }
}