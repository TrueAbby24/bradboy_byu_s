import React, { Component } from "react";
import { 
    View, 
    Text, 
    Button, 
    TouchableOpacity, 
    TextInput, 
    StyleSheet, 
    Alert, 
    ScrollView,
    Image,
 } from 'react-native';
import { Icon, Container, Header, Content, Left, Title , Right } from "native-base"
import { DrawerActions } from 'react-navigation';


export default class PlayScreen extends React.Component{

    static navigationOptions = {
        title: 'Play',
        headerMode: 'screen',
        cardStyle: { backgroundColor: '#6ab04c' },
      };

    render(){
        return(
            <Container>
            <Header>
                  <Left>
                      <Icon name="ios-menu" onPress={() => 
                          this.props.navigation.dispatch(DrawerActions.openDrawer())} />
                  </Left>
                  <Title style={styles.title}>Play</Title>
                    <Right />
              </Header>
              <Content contentContainerStyle={styles.container}>
              <TouchableOpacity
                  style={styles.button}
                  onPress={() => navigate('T20Screen')}>
                <Text style={styles.buttonText}>T20</Text>
                </TouchableOpacity>
                
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => navigate('T20Screen')}>
                <Text style={styles.buttonText}>ODI</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.button}
                  onPress={() => navigate('T20Screen')}>
                <Text style={styles.buttonText}>Test</Text>
                </TouchableOpacity>

                  </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#6ab04c',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
        alignContent: "center",
        alignItems: "center", 
        textAlign: "center"
    },
    titleText:{
        //fontFamily: 'Baskerville',
        marginTop: 20,
        fontSize: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'powderblue',
        width: '70%',
        height: 50,
        padding: 10,
        marginTop: 10,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 25,
        marginBottom: 10,
    },
    buttonText:{
        //fontFamily: 'Baskerville',
        fontSize: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        width: '70%',
        //fontFamily: 'Baskerville',
        fontSize: 20,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 25,
        marginVertical: 10,
    },
    space: {
        width: 200,
        height: 25,
        padding: 0,
    },
    bigSpace: {
      width: 200,
      height: 100,
      padding: 0,
  },
    center: {
        flex: 0,
        alignItems: 'center',
        backgroundColor: '#6ab04c',
        width: '100%',
    },
    title: {
        alignSelf: "center",
    }

});


