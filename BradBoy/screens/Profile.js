import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView, Image, TouchableOpacity} from "react-native"
import { Icon, Button, Container, Header, Content, Left, Title , Right} from "native-base"
import { DrawerActions } from 'react-navigation'
import { Avatar } from 'react-native-elements';

//let width = 0;
let height = 100;

export default class ProfileScreen extends React.Component {
    state = {
        visibility :false
    }
    
    render () {
        let avatars;
        editAvatar = () => {
            console.log('hey');
            if(this.state.visibility){
                console.log('true');
                this.setState({visibility: false});
            } else {
                console.log('false');
                this.setState({visibility: true});
            }
        }
        changeAvatar = () => {
            console.log('hello');
        }
        if(this.state.visibility){
            avatars = 
                <ScrollView>
                    <TouchableOpacity onPress={changeAvatar(this)}>
                        <Image
                        style={{width: 100, height: 100}}
                        source={require('../assets/images/avatar_boy_blue.png')}
                        onPress={this.changeAvatar}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image
                        style={{width: 100, height: 100}}
                        source={require('../assets/images/avatar_boy_green.png')}
                        onPress={this.changeAvatar}
                        />
                    </TouchableOpacity>
                    <Image
                    style={{width: 100, height: 100}}
                    source={require('../assets/images/avatar_boy_red.png')}
                    onPress={this.changeAvatar}
                    />
                    <Image
                    style={{width: 100, height: 100}}
                    source={require('../assets/images/avatar_boy_yellow.png')}
                    onPress={this.changeAvatar}
                    />
                    <Image
                    style={{width: 100, height: 100}}
                    source={require('../assets/images/avatar_girl_blue.png')}
                    onPress={this.changeAvatar}
                    />
                    <Image
                    style={{width: 100, height: 100}}
                    source={require('../assets/images/avatar_girl_green.png')}
                    onPress={this.changeAvatar}
                    />
                    <Image
                    style={{width: 100, height: 100}}
                    source={require('../assets/images/avatar_girl_red.png')}
                    onPress={this.changeAvatar}
                    />
                    <Image
                    style={{width: 100, height: 100}}
                    source={require('../assets/images/avatar_girl_yellow.png')}
                    onPress={this.changeAvatar}
                    />
                </ScrollView>
        } else {
            avatars = <Text></Text>;
        }
        
        
    
        return (
            <Container>
                <Header>
                    <Left>
                        <Icon name="ios-menu" onPress={() => 
                            this.props.navigation.dispatch(DrawerActions.openDrawer())} />
                    </Left>
                    <Title style={styles.title}>Profile</Title>
                    <Right />
                </Header>
                <Content contentContainerStyle={styles.container}>
                    <Avatar
                        size="xlarge"
                        source={require('../assets/images/avatar_boy_blue.png')}
                        showEditButton
                        onPress={this.changeAvatar}
                        editButton={{ onPress: () => editAvatar(this) }}
                    />
                    <View style={{height: 100}}>
                        {avatars}
                    </View>
                    <Text style={styles.text}>This is the Profile screen :)</Text>                   
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center'
    },
    text: {
        alignContent: "center",
        alignItems: "center", 
        textAlign: "center"
    },
    title: {
        alignSelf: "center"
    }
  });