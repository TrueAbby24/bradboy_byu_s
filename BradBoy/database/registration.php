<?php
include 'dbconfig.php';
 
// Creating connection.
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
 
// Getting the received JSON into $json variable.
$json = file_get_contents('php://input');
 
// decoding the received JSON and store into $obj variable.
$obj = json_decode($json,true);
 
 // Populate User name from JSON $obj array and store into $name.
//$name = $obj['name'];
 
// Populate User email from JSON $obj array and store into $email.
$email = $obj['email'];
 
// Populate Password from JSON $obj array and store into $password.
$password = $obj['password'];
 
//Checking Email is already exist or not using SQL query.
$CheckSQL = "SELECT * FROM Player WHERE email='$email'";
 
// Executing SQL Query.
$check = mysqli_fetch_array(mysqli_query($con,$CheckSQL));
 
 
if(isset($check)){
    $EmailExistMSG = 'Failed: e-Mail already exists.';
    
    // Converting the message into JSON format.
    $EmailExistJson = json_encode($EmailExistMSG);
    
    // Echo the message.
    echo $EmailExistJson ; 
} else{
    // Creating SQL query and insert the record into MySQL database table.
    $Sql_Query = "insert into Player (email,password) values ('$email','$password')";
    
    if(mysqli_query($con,$Sql_Query)){
        $MSG = 'User Registered Successfully' ;
        $json = json_encode($MSG);
        echo $json ;
    } else{
        $MSG = 'Try Again' ;
        $json = json_encode($MSG);
        echo $json;
    }
}
mysqli_close($con);
?>