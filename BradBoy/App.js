import React, { Component } from "react";
import { AppLoading } from "expo";
import * as Font from 'expo-font';
import { 
  View, 
  Text, 
  Button, 
  TouchableOpacity, 
  TextInput, 
  StyleSheet, 
  Alert, 
  ScrollView,
  Image,
  Title
} from 'react-native';
import { createDrawerNavigator, createAppContainer, DrawerItems } from 'react-navigation'
import HomeScreen from "./screens/HomeScreen"
import SettingsScreen from "./screens/SettingsScreen"
import LoginScreen from "./screens/LoginScreen"
import LandingScreen from "./screens/LandingScreen";
import { Container, Content, Header, Body, Icon, Left } from 'native-base'
import TrainScreen from './screens/TrainScreen'
import PlayScreen from './screens/PlayScreen'
import T20Screen from "./screens/T20Screen"
import ProfileScreen from "./screens/Profile";


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return (
          <AppLoading />
      );
    }
    return (
        <MyApp />
    );
  }
}


const CustomDrawerContentComponent = (props) => (
  <Container>
    <Header style={{ height: 200, backgroundColor: 'white'}}>
      <Body>
        <Image
        style={styles.drawerImage}
        source={require('./assets/images/bradboy.png')}/>
      </Body>
    </Header>
    <Content>
      <DrawerItems {...props} />
    </Content>
  </Container>
)


const AppNav = createDrawerNavigator({
  Home: {
    screen: HomeScreen
  },
  Login: {
    screen: LoginScreen,
    navigationOptions: () => ({
      drawerLockMode: 'locked-closed',
    })
  },
  Profile: {
    screen: ProfileScreen
  },
  Settings: {
    screen: SettingsScreen
  }, 
  Landing: {
    screen: LandingScreen,
    navigationOptions: () => ({
      drawerLockMode: 'locked-closed',
    })
  },

  Train:
  {
    screen: TrainScreen
  },

  Play:
  {
    screen: PlayScreen
  },
},{
  initialRouteName: 'Landing',
  drawerPosition: 'left',
  contentComponent: CustomDrawerContentComponent,
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute: ' DrawerToggle'
})

const MyApp = createAppContainer(AppNav);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffbe76',
    alignItems: 'center',
    justifyContent: 'center',
  },
  drawerImage: {
    height: 150,
    width: 150,
  },

  T20: {
    alignContent: "flex-end",
    
  }
});
