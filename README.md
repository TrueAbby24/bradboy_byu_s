# Bradboy
This is the Bradboy app repo!

---

## Install dependencies
You must have Node.Js already installed, preferrably version 10.11.X

`cd` to your project directory and then in your terminal, type:

1. `npm install --save react-native-elements`
2. `npm install 'react-navigation'`
3. `npm install 'native-base'`
4. `npm install react-native-gesture-handler react-native-reanimated`
5. `npm install 'react-native-table-component'`
6. `npm install '@expo/vector-icons'`


---

## Run the app
To run the app you must follow the steps on https://www.expo.io first. Then:

1. `cd` to the project directory
2. Type `expo start` to start the app in development mode

